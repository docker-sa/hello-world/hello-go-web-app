
# Deploy the application to K8s
> open a terminal with K9S: `k9s --all-namespaces`

## First create a `demo` namespace:

```bash
cd k8s
kubectl create namespace demo --dry-run=client -o yaml | kubectl apply -f -
```

## First Deployment

📝 **open** `deploy.yaml`
> This Kubernetes manifest describes three main resources: a **Service**, a **Deployment**, and an **Ingress**.


To deploy the application to Kubenetes, type the following commands:
```bash
# Type the following commands in another terminal
kubectl apply -f deploy.yaml -n demo
# 👋 show the logs

kubectl describe ingress -n demo
# go to the webapp: http://go-webapp-demo.0.0.0.0.nip.io/

# Change Environment variables
kubectl apply -f deploy.yaml -n demo
# go to the webapp: http://go-webapp-demo.0.0.0.0.nip.io/
```

## Deployment with config map

**FIRST**:
✋ Change the environment variables in the `deploy.yaml` file.

```bash
# Create the config map
kubectl apply -f config.yaml -n demo

# Update the deploy manifest
kubectl apply -f deploy.yaml -n demo

# 👋 show the logs

# go to the webapp: http://go-webapp-demo.0.0.0.0.nip.io/

```

**AT THE END**:
```bash
# Remove everything
kubectl delete namespace demo
```
