---
# Service
# ----------------
# The Service is a way to expose the application running in the pods 
# to the external world or other services within the cluster.

# A pod is the smallest and simplest unit in Kubernetes that you can create or deploy. 
# A pod is a single instance of a running process in your Kubernetes cluster. 
# It can contain one or more containers.

apiVersion: v1
kind: Service
metadata:
  name: go-webapp-demo
spec:
  selector:
    app: go-webapp-demo
  ports:
    - port: 80
      targetPort: 8080
---
# Deployment
# ----------------
# The Deployment is a way to manage the pods running the application.
# The Deployment manages a set of identical pods, 
# ensuring that a specified number of pods are running and updated as needed.
apiVersion: apps/v1
kind: Deployment
metadata:
  name: go-webapp-demo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: go-webapp-demo
  template:
    metadata:
      labels:
        app: go-webapp-demo
    spec:
      containers:
        - name: go-webapp-demo
          image: philippecharriere494/go-webapp-demo:0.0.5
          env:
          - name: MESSAGE
            value: "👋 Hello World 🌍 from ☸️ K8S"
          #- name: DB_CONFIG_FILE
          #  value: "/data/db-config.txt"
          ports:
            - containerPort: 8080
          #imagePullPolicy: Always
          imagePullPolicy: IfNotPresent
          #volumeMounts:
          #  - name: config-volume
          #    mountPath: /data
      #volumes:
      #  - name: config-volume
      #    configMap:
      #      name: demo-config
---
# Ingress
# ----------------
# The Ingress is a way to expose the application to the external world.
# The Ingress resource manages external access to the services, typically HTTP.
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: go-webapp-demo
  labels:
spec:
  rules:
    - host: go-webapp-demo.0.0.0.0.nip.io
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service: 
              name: go-webapp-demo
              port: 
                number: 80

# nip.io is a free wildcard DNS service that maps IP addresses to domain names.
# It is a great service to use for testing purposes.
# When setting up Ingress resources in Kubernetes, 
# you can use nip.io to quickly create domain names 
# that point to the IP addresses of your services, 
# making it easier to access them without additional DNS configuration.