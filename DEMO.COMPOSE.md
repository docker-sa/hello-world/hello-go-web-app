# Run the application with Compose

> 👀 look at `./compose.yaml`

```bash
docker compose up
```
> - 👀📝 see the logs
> - Open 🐳 Docker Desktop and see **Bind Mounds** (or -> http://localhost:6065/)


```bash
docker compose down
```