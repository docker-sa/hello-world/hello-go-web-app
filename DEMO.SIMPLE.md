# Run the application with Docker CLI

## My web application

> 👀 look at `webapp/main.go`

This is a simple web application that listens on port `8080` and serves a simple message.

It uses the `HTTP_PORT` environment variable to set the port to listen on.
It uses the `MESSAGE` environment variable to set the message to serve.

**About the DB Config Status and Config Handling**:

- `dbConfigStatus`: Variable to store the db config status.
- `dbConfigFile`: Gets the path of the config file from environment variables (`DB_CONFIG_FILE`).

👋 If the file exists and its content is `this-is-the-db-config`, configuration is successful; otherwise, it fails.

## Build the application

> 👀 look at `webapp/Dockerfile`

```bash
cd webapp
docker build -t go-web-app:demo . 
# check
docker images | grep go-web-app
```

## Run the application with env variables

```bash
docker run -p 6060:6060 \
-e "HTTP_PORT=6060" \
-e "MESSAGE=👋 Hello World 🌍" \
go-web-app:demo
```
> - 👀📝 see the logs
> - Open 🐳 Docker Desktop (or -> http://localhost:6060/)

## Run the application with a password file

```bash
# make a pointer to the file
# mount the file 

docker run -p 6062:6062 \
-e "HTTP_PORT=6062" \
-e "MESSAGE=👋 Hello Docker 🐳" \
-e DB_CONFIG_FILE=/config/db-config.txt \
-v ./db-config.txt:/config/db-config.txt \
go-web-app:demo
```
> - 👀📝 see the logs
> - Open 🐳 Docker Desktop and see **Bind Mounds** (or -> http://localhost:6062/)
