# hello-go-web-app

You can use this demo to demonstrate the use of environment variables and files for forwarding configuration (or password file) to a Go web application.

## Demo with Docker Compose


## Demo with Kubernetes
> more details to use the Kubernetes in Docker: https://gitlab.com/docker-sa/k8s/k-n-d-hello-world

### Prerequisites

> Build and push the Docker image:
```bash
docker buildx build \
--platform=linux/arm64 \
--push -t philippecharriere494/go-webapp-demo:0.0.4 .
```

### K8S Setup

**Connect to the cluster**:
```bash
kubectl config get-contexts
kubectl config use-context docker-desktop
kubectl cluster-info
```

**Connect to the cluster with K9s**:
```bash
k9s --all-namespaces
```

**Install Traefik**:
```bash
helm repo add traefik https://traefik.github.io/charts
helm repo update
helm install traefik traefik/traefik

kubectl get services --all-namespaces
```

### Deploy the application to K8s

First create a `demo` namespace:

```bash
kubectl create namespace demo --dry-run=client -o yaml | kubectl apply -f -
```

#### First Deployment
```bash
cd k8s
# Type the following commands in another terminal
kubectl apply -f deploy.yaml -n demo
# 👋 show the logs

kubectl describe ingress -n demo
# go to the webapp: http://go-webapp-demo.0.0.0.0.nip.io/

# Change Environment variables
kubectl apply -f deploy.yaml -n demo
# go to the webapp: http://go-webapp-demo.0.0.0.0.nip.io/
```

#### Deployment with config map

**FIRST**:
✋ Change the environment variables in the `deploy.yaml` file.

```bash
# Create the config map
kubectl apply -f config.yaml -n demo

# Update the deploy manifest
kubectl apply -f deploy.yaml -n demo

# 👋 show the logs

# go to the webapp: http://go-webapp-demo.0.0.0.0.nip.io/

```

**AT THE END**:
```bash
# Remove everything
kubectl delete namespace demo
```


## Tips

> **good to know:** ✋ How to fix `permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock`:
> ```bash
> sudo chmod 666 /var/run/docker.sock
> ```

