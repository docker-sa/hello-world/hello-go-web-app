#!/bin/bash

# make a pointer to the file
# mount the file 

docker run -p 6062:6062 \
-e HTTP_PORT=6062 \
-e "MESSAGE=👋 Hello from 📦 + 🔐" \
-e DB_CONFIG_FILE=/config/db-config.txt \
-v ./db-config.txt:/config/db-config.txt \
go-web-app:demo
