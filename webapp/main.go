// Package main : a simple web app
package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {

	var httpPort = os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}

	var message = os.Getenv("MESSAGE")
	if message == "" {
		message = "this is a message"
	}

	log.Println("🚀 starting web server on port: " + httpPort)
	log.Println("📝 message: " + message)

	var dbConfigStatus = ""
	var dbConfigFile = os.Getenv("DB_CONFIG_FILE")

	log.Println("dbConfigFile", dbConfigFile)

	if dbConfigFile == "" {

		dbConfigStatus = "😡 no database config file"

		log.Println("dbConfigFile empty", dbConfigStatus)

	} else {

		content, err := os.ReadFile(dbConfigFile)
		if err != nil {
			dbConfigStatus = "😡 database config file issue: " + err.Error()

			log.Println("dbConfigStatus error", dbConfigStatus)

		}

		log.Println("📝 dbConfigFile content", content, string(content))

		if strings.Replace(string(content), "\n", "", -1) == "this-is-the-db-config" {
			dbConfigStatus = "🙂 database config ✅"
		} else {
			dbConfigStatus = "😡 database config 👎"

			log.Println("false content", dbConfigStatus)

		}
	}

	mux := http.NewServeMux()

	fileServerHtml := http.FileServer(http.Dir("public"))
	mux.Handle("/", fileServerHtml)

	mux.HandleFunc("/variables", func(response http.ResponseWriter, request *http.Request) {

		variables := map[string]interface{}{
			"message":              message,
			"authenticationStatus": dbConfigStatus,
		}

		jsonString, err := json.Marshal(variables)

		if err != nil {
			response.WriteHeader(http.StatusNoContent)
		}

		response.Header().Set("Content-Type", "application/json; charset=utf-8")
		response.WriteHeader(http.StatusOK)
		response.Write(jsonString)
	})

	var errListening error
	log.Println("🌍 http server is listening on: " + httpPort)
	errListening = http.ListenAndServe(":"+httpPort, mux)

	log.Fatal(errListening)
}
